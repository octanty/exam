defmodule PracticeExamWeb.ParkingsControllerTest do
  use PracticeExamWeb.ConnCase

  alias PracticeExam.Parking

  @create_attrs %{parkings_name: "some parkings_name", place: "some place"}
  @update_attrs %{parkings_name: "some updated parkings_name", place: "some updated place"}
  @invalid_attrs %{parkings_name: nil, place: nil}

  def fixture(:parkings) do
    {:ok, parkings} = Parking.create_parkings(@create_attrs)
    parkings
  end

  describe "index" do
    test "lists all oarkings", %{conn: conn} do
      conn = get conn, parkings_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Oarkings"
    end
  end

  describe "new parkings" do
    test "renders form", %{conn: conn} do
      conn = get conn, parkings_path(conn, :new)
      assert html_response(conn, 200) =~ "New Parkings"
    end
  end

  describe "create parkings" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, parkings_path(conn, :create), parkings: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == parkings_path(conn, :show, id)

      conn = get conn, parkings_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Parkings"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, parkings_path(conn, :create), parkings: @invalid_attrs
      assert html_response(conn, 200) =~ "New Parkings"
    end
  end

  describe "edit parkings" do
    setup [:create_parkings]

    test "renders form for editing chosen parkings", %{conn: conn, parkings: parkings} do
      conn = get conn, parkings_path(conn, :edit, parkings)
      assert html_response(conn, 200) =~ "Edit Parkings"
    end
  end

  describe "update parkings" do
    setup [:create_parkings]

    test "redirects when data is valid", %{conn: conn, parkings: parkings} do
      conn = put conn, parkings_path(conn, :update, parkings), parkings: @update_attrs
      assert redirected_to(conn) == parkings_path(conn, :show, parkings)

      conn = get conn, parkings_path(conn, :show, parkings)
      assert html_response(conn, 200) =~ "some updated parkings_name"
    end

    test "renders errors when data is invalid", %{conn: conn, parkings: parkings} do
      conn = put conn, parkings_path(conn, :update, parkings), parkings: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Parkings"
    end
  end

  describe "delete parkings" do
    setup [:create_parkings]

    test "deletes chosen parkings", %{conn: conn, parkings: parkings} do
      conn = delete conn, parkings_path(conn, :delete, parkings)
      assert redirected_to(conn) == parkings_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, parkings_path(conn, :show, parkings)
      end
    end
  end

  defp create_parkings(_) do
    parkings = fixture(:parkings)
    {:ok, parkings: parkings}
  end
end
