defmodule PracticeExam.ParkingTest do
  use PracticeExam.DataCase

  alias PracticeExam.Parking

  describe "oarkings" do
    alias PracticeExam.Parking.Parkings

    @valid_attrs %{parkings_name: "some parkings_name", place: "some place"}
    @update_attrs %{parkings_name: "some updated parkings_name", place: "some updated place"}
    @invalid_attrs %{parkings_name: nil, place: nil}

    def parkings_fixture(attrs \\ %{}) do
      {:ok, parkings} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Parking.create_parkings()

      parkings
    end

    test "list_oarkings/0 returns all oarkings" do
      parkings = parkings_fixture()
      assert Parking.list_oarkings() == [parkings]
    end

    test "get_parkings!/1 returns the parkings with given id" do
      parkings = parkings_fixture()
      assert Parking.get_parkings!(parkings.id) == parkings
    end

    test "create_parkings/1 with valid data creates a parkings" do
      assert {:ok, %Parkings{} = parkings} = Parking.create_parkings(@valid_attrs)
      assert parkings.parkings_name == "some parkings_name"
      assert parkings.place == "some place"
    end

    test "create_parkings/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Parking.create_parkings(@invalid_attrs)
    end

    test "update_parkings/2 with valid data updates the parkings" do
      parkings = parkings_fixture()
      assert {:ok, parkings} = Parking.update_parkings(parkings, @update_attrs)
      assert %Parkings{} = parkings
      assert parkings.parkings_name == "some updated parkings_name"
      assert parkings.place == "some updated place"
    end

    test "update_parkings/2 with invalid data returns error changeset" do
      parkings = parkings_fixture()
      assert {:error, %Ecto.Changeset{}} = Parking.update_parkings(parkings, @invalid_attrs)
      assert parkings == Parking.get_parkings!(parkings.id)
    end

    test "delete_parkings/1 deletes the parkings" do
      parkings = parkings_fixture()
      assert {:ok, %Parkings{}} = Parking.delete_parkings(parkings)
      assert_raise Ecto.NoResultsError, fn -> Parking.get_parkings!(parkings.id) end
    end

    test "change_parkings/1 returns a parkings changeset" do
      parkings = parkings_fixture()
      assert %Ecto.Changeset{} = Parking.change_parkings(parkings)
    end
  end
end
