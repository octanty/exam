# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :practice_exam,
  ecto_repos: [PracticeExam.Repo]

# Configures the endpoint
config :practice_exam, PracticeExamWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "/sdyaq5aD/IiLTZAR1F+/0pmlLKocEAEcvtFg1Q0tDHvCTvuICEsmsZb0gHM9Dpp",
  render_errors: [view: PracticeExamWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PracticeExam.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
