use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :practice_exam, PracticeExamWeb.Endpoint,
  http: [port: 4001],
  server: false

config :hound, driver: "chrome_driver"
config :takso, sql_sandbox: true
# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :practice_exam, PracticeExam.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "practice_exam_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
