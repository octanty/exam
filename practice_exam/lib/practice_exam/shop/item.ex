defmodule PracticeExam.Shop.Item do
  use Ecto.Schema
  import Ecto.Changeset


  schema "items" do
    field :amount, :string
    field :item_name, :string

    timestamps()
  end

  @doc false
  def changeset(item, attrs) do
    item
    |> cast(attrs, [:item_name, :amount])
    |> validate_required([:item_name, :amount])
  end
end
