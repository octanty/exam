defmodule PracticeExam.Parking do
  @moduledoc """
  The Parking context.
  """

  import Ecto.Query, warn: false
  alias PracticeExam.Repo

  alias PracticeExam.Parking.Parkings

  @doc """
  Returns the list of oarkings.

  ## Examples

      iex> list_oarkings()
      [%Parkings{}, ...]

  """
  def list_oarkings do
    Repo.all(Parkings)
  end

  @doc """
  Gets a single parkings.

  Raises `Ecto.NoResultsError` if the Parkings does not exist.

  ## Examples

      iex> get_parkings!(123)
      %Parkings{}

      iex> get_parkings!(456)
      ** (Ecto.NoResultsError)

  """
  def get_parkings!(id), do: Repo.get!(Parkings, id)

  @doc """
  Creates a parkings.

  ## Examples

      iex> create_parkings(%{field: value})
      {:ok, %Parkings{}}

      iex> create_parkings(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_parkings(attrs \\ %{}) do
    %Parkings{}
    |> Parkings.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a parkings.

  ## Examples

      iex> update_parkings(parkings, %{field: new_value})
      {:ok, %Parkings{}}

      iex> update_parkings(parkings, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_parkings(%Parkings{} = parkings, attrs) do
    parkings
    |> Parkings.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Parkings.

  ## Examples

      iex> delete_parkings(parkings)
      {:ok, %Parkings{}}

      iex> delete_parkings(parkings)
      {:error, %Ecto.Changeset{}}

  """
  def delete_parkings(%Parkings{} = parkings) do
    Repo.delete(parkings)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking parkings changes.

  ## Examples

      iex> change_parkings(parkings)
      %Ecto.Changeset{source: %Parkings{}}

  """
  def change_parkings(%Parkings{} = parkings) do
    Parkings.changeset(parkings, %{})
  end
end
