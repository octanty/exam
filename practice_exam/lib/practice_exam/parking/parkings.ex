defmodule PracticeExam.Parking.Parkings do
  use Ecto.Schema
  import Ecto.Changeset


  schema "oarkings" do
    field :parkings_name, :string
    field :place, :string

    timestamps()
  end

  @doc false
  def changeset(parkings, attrs) do
    parkings
    |> cast(attrs, [:parkings_name, :place])
    |> validate_required([:parkings_name, :place])
  end
end
