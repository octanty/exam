defmodule PracticeExamWeb.ParkingsController do
  use PracticeExamWeb, :controller

  alias PracticeExam.Parking
  alias PracticeExam.Parking.Parkings

  def index(conn, _params) do
    oarkings = Parking.list_oarkings()
    render(conn, "index.html", oarkings: oarkings)
  end

  def new(conn, _params) do
    changeset = Parking.change_parkings(%Parkings{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"parkings" => parkings_params}) do
    case Parking.create_parkings(parkings_params) do
      {:ok, parkings} ->
        conn
        |> put_flash(:info, "Parkings created successfully.")
        |> redirect(to: parkings_path(conn, :show, parkings))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    parkings = Parking.get_parkings!(id)
    render(conn, "show.html", parkings: parkings)
  end

  def edit(conn, %{"id" => id}) do
    parkings = Parking.get_parkings!(id)
    changeset = Parking.change_parkings(parkings)
    render(conn, "edit.html", parkings: parkings, changeset: changeset)
  end

  def update(conn, %{"id" => id, "parkings" => parkings_params}) do
    parkings = Parking.get_parkings!(id)

    case Parking.update_parkings(parkings, parkings_params) do
      {:ok, parkings} ->
        conn
        |> put_flash(:info, "Parkings updated successfully.")
        |> redirect(to: parkings_path(conn, :show, parkings))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", parkings: parkings, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    parkings = Parking.get_parkings!(id)
    {:ok, _parkings} = Parking.delete_parkings(parkings)

    conn
    |> put_flash(:info, "Parkings deleted successfully.")
    |> redirect(to: parkings_path(conn, :index))
  end
end
