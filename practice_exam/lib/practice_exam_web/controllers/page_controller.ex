defmodule PracticeExamWeb.PageController do
  use PracticeExamWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
