defmodule PracticeExamWeb.Router do
  use PracticeExamWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PracticeExamWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    resources "/parkings", ParkingsController
    resources "/users", UserController
    resources "/shop", ItemController
  end

  # Other scopes may use custom stacks.
  # scope "/api", PracticeExamWeb do
  #   pipe_through :api
  # end
end
