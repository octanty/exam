defmodule PracticeExam.Repo.Migrations.CreateItems do
  use Ecto.Migration

  def change do
    create table(:items) do
      add :item_name, :string
      add :amount, :string

      timestamps()
    end

  end
end
