defmodule PracticeExam.Repo.Migrations.CreateOarkings do
  use Ecto.Migration

  def change do
    create table(:oarkings) do
      add :parkings_name, :string
      add :place, :string

      timestamps()
    end

  end
end
