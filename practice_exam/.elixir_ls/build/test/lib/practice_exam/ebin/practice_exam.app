{application,practice_exam,
             [{applications,[kernel,stdlib,elixir,logger,runtime_tools,
                             gettext,pbkdf2_elixir,white_bread,hound,
                             phoenix_pubsub,cowboy,postgrex,phoenix,
                             phoenix_html,guardian,phoenix_ecto]},
              {description,"practice_exam"},
              {modules,['Elixir.PracticeExam','Elixir.PracticeExam.Accounts',
                        'Elixir.PracticeExam.Accounts.User',
                        'Elixir.PracticeExam.Application',
                        'Elixir.PracticeExam.DataCase',
                        'Elixir.PracticeExam.Parking',
                        'Elixir.PracticeExam.Parking.Parkings',
                        'Elixir.PracticeExam.Repo','Elixir.PracticeExam.Shop',
                        'Elixir.PracticeExam.Shop.Item',
                        'Elixir.PracticeExamWeb',
                        'Elixir.PracticeExamWeb.ChannelCase',
                        'Elixir.PracticeExamWeb.ConnCase',
                        'Elixir.PracticeExamWeb.Endpoint',
                        'Elixir.PracticeExamWeb.ErrorHelpers',
                        'Elixir.PracticeExamWeb.ErrorView',
                        'Elixir.PracticeExamWeb.Gettext',
                        'Elixir.PracticeExamWeb.ItemController',
                        'Elixir.PracticeExamWeb.ItemView',
                        'Elixir.PracticeExamWeb.LayoutView',
                        'Elixir.PracticeExamWeb.PageController',
                        'Elixir.PracticeExamWeb.PageView',
                        'Elixir.PracticeExamWeb.ParkingsController',
                        'Elixir.PracticeExamWeb.ParkingsView',
                        'Elixir.PracticeExamWeb.Router',
                        'Elixir.PracticeExamWeb.Router.Helpers',
                        'Elixir.PracticeExamWeb.UserController',
                        'Elixir.PracticeExamWeb.UserSocket',
                        'Elixir.PracticeExamWeb.UserView']},
              {registered,[]},
              {vsn,"0.0.1"},
              {mod,{'Elixir.PracticeExam.Application',[]}}]}.
