defmodule Rating.Repo.Migrations.CreateRatings do
  use Ecto.Migration

  def change do
    create table(:ratings) do
      add :Product, :string
      add :Name, :string
      add :Quantity, :string
      add :Votes, :string
      add :Average_Rate, :string

      timestamps()
    end

  end
end
