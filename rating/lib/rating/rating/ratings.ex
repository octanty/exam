defmodule Rating.Rating.Ratings do
  use Ecto.Schema
  import Ecto.Changeset


  schema "ratings" do
    field :Average_Rate, :string
    field :Name, :string
    field :Product, :string
    field :Quantity, :string
    field :Votes, :string

    timestamps()
  end

  @doc false
  def changeset(ratings, attrs) do
    ratings
    |> cast(attrs, [:Product, :Name, :Quantity, :Votes, :Average_Rate])
    |> validate_required([:Product, :Name, :Quantity, :Votes, :Average_Rate])
  end
end
