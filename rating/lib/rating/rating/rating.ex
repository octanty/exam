defmodule Rating.Rating do
  @moduledoc """
  The Rating context.
  """

  import Ecto.Query, warn: false
  alias Rating.Repo

  alias Rating.Rating.Ratings

  @doc """
  Returns the list of ratings.

  ## Examples

      iex> list_ratings()
      [%Ratings{}, ...]

  """
  def list_ratings do
    Repo.all(Ratings)
  end

  @doc """
  Gets a single ratings.

  Raises `Ecto.NoResultsError` if the Ratings does not exist.

  ## Examples

      iex> get_ratings!(123)
      %Ratings{}

      iex> get_ratings!(456)
      ** (Ecto.NoResultsError)

  """
  def get_ratings!(id), do: Repo.get!(Ratings, id)

  @doc """
  Creates a ratings.

  ## Examples

      iex> create_ratings(%{field: value})
      {:ok, %Ratings{}}

      iex> create_ratings(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_ratings(attrs \\ %{}) do
    %Ratings{}
    |> Ratings.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a ratings.

  ## Examples

      iex> update_ratings(ratings, %{field: new_value})
      {:ok, %Ratings{}}

      iex> update_ratings(ratings, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_ratings(%Ratings{} = ratings, attrs) do
    ratings
    |> Ratings.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Ratings.

  ## Examples

      iex> delete_ratings(ratings)
      {:ok, %Ratings{}}

      iex> delete_ratings(ratings)
      {:error, %Ecto.Changeset{}}

  """
  def delete_ratings(%Ratings{} = ratings) do
    Repo.delete(ratings)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking ratings changes.

  ## Examples

      iex> change_ratings(ratings)
      %Ecto.Changeset{source: %Ratings{}}

  """
  def change_ratings(%Ratings{} = ratings) do
    Ratings.changeset(ratings, %{})
  end
end
