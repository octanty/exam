defmodule RatingWeb.PageController do
  use RatingWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
