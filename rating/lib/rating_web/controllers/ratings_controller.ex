defmodule RatingWeb.RatingsController do
  use RatingWeb, :controller

  alias Rating.Rating
  alias Rating.Rating.Ratings

  def index(conn, _params) do
    ratings = Rating.list_ratings()
    render(conn, "index.html", ratings: ratings)
  end

  def new(conn, _params) do
    changeset = Rating.change_ratings(%Ratings{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"ratings" => ratings_params}) do
    case Rating.create_ratings(ratings_params) do
      {:ok, ratings} ->
        conn
        |> put_flash(:info, "Ratings created successfully.")
        |> redirect(to: ratings_path(conn, :show, ratings))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    ratings = Rating.get_ratings!(id)
    render(conn, "show.html", ratings: ratings)
  end

  def edit(conn, %{"id" => id}) do
    ratings = Rating.get_ratings!(id)
    changeset = Rating.change_ratings(ratings)
    render(conn, "edit.html", ratings: ratings, changeset: changeset)
  end

  def update(conn, %{"id" => id, "ratings" => ratings_params}) do
    ratings = Rating.get_ratings!(id)

    case Rating.update_ratings(ratings, ratings_params) do
      {:ok, ratings} ->
        conn
        |> put_flash(:info, "Ratings updated successfully.")
        |> redirect(to: ratings_path(conn, :show, ratings))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", ratings: ratings, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    ratings = Rating.get_ratings!(id)
    {:ok, _ratings} = Rating.delete_ratings(ratings)

    conn
    |> put_flash(:info, "Ratings deleted successfully.")
    |> redirect(to: ratings_path(conn, :index))
  end
end
