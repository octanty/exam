defmodule Rating.RatingTest do
  use Rating.DataCase

  alias Rating.Rating

  describe "ratings" do
    alias Rating.Rating.Ratings

    @valid_attrs %{Average_Rate: "some Average_Rate", Name: "some Name", Product: "some Product", Quantity: "some Quantity", Votes: "some Votes"}
    @update_attrs %{Average_Rate: "some updated Average_Rate", Name: "some updated Name", Product: "some updated Product", Quantity: "some updated Quantity", Votes: "some updated Votes"}
    @invalid_attrs %{Average_Rate: nil, Name: nil, Product: nil, Quantity: nil, Votes: nil}

    def ratings_fixture(attrs \\ %{}) do
      {:ok, ratings} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Rating.create_ratings()

      ratings
    end

    test "list_ratings/0 returns all ratings" do
      ratings = ratings_fixture()
      assert Rating.list_ratings() == [ratings]
    end

    test "get_ratings!/1 returns the ratings with given id" do
      ratings = ratings_fixture()
      assert Rating.get_ratings!(ratings.id) == ratings
    end

    test "create_ratings/1 with valid data creates a ratings" do
      assert {:ok, %Ratings{} = ratings} = Rating.create_ratings(@valid_attrs)
      assert ratings.Average_Rate == "some Average_Rate"
      assert ratings.Name == "some Name"
      assert ratings.Product == "some Product"
      assert ratings.Quantity == "some Quantity"
      assert ratings.Votes == "some Votes"
    end

    test "create_ratings/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Rating.create_ratings(@invalid_attrs)
    end

    test "update_ratings/2 with valid data updates the ratings" do
      ratings = ratings_fixture()
      assert {:ok, ratings} = Rating.update_ratings(ratings, @update_attrs)
      assert %Ratings{} = ratings
      assert ratings.Average_Rate == "some updated Average_Rate"
      assert ratings.Name == "some updated Name"
      assert ratings.Product == "some updated Product"
      assert ratings.Quantity == "some updated Quantity"
      assert ratings.Votes == "some updated Votes"
    end

    test "update_ratings/2 with invalid data returns error changeset" do
      ratings = ratings_fixture()
      assert {:error, %Ecto.Changeset{}} = Rating.update_ratings(ratings, @invalid_attrs)
      assert ratings == Rating.get_ratings!(ratings.id)
    end

    test "delete_ratings/1 deletes the ratings" do
      ratings = ratings_fixture()
      assert {:ok, %Ratings{}} = Rating.delete_ratings(ratings)
      assert_raise Ecto.NoResultsError, fn -> Rating.get_ratings!(ratings.id) end
    end

    test "change_ratings/1 returns a ratings changeset" do
      ratings = ratings_fixture()
      assert %Ecto.Changeset{} = Rating.change_ratings(ratings)
    end
  end
end
