Feature: Want to Rate Product
  As a user
  Such that I want to select a product to rate

  Scenario: Rate product via web page 
    Given the following data user
          | user_email | Score	       | 
          | Juhan      | 5             | 
         
    And I click rate button in Laptop HP-43 
    And I go to page rate product
    And I enter user email
    And I enter score
    When I click save
    Then I go to main page