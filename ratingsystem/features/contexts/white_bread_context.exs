defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  alias Takso.{Repo, Sales.Taxi}

  feature_starting_state(fn ->
    Application.ensure_all_started(:hound)
    %{}
  end)

  scenario_starting_state(fn state ->
    Hound.start_session()
    Ecto.Adapters.SQL.Sandbox.checkout(RatingSystem.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(RatingSystem.Repo, {:shared, self()})
    %{}
  end)

  scenario_finalize(fn _status, _state ->
    #Ecto.Adapters.SQL.Sandbox.checkin(Takso.Repo)
    #Hound.end_session()

    nil
  end)



  given_ ~r/^the following the following data user$/, fn state, %{table_data: list} ->
    state = Enum.at(list, 0)
    {:ok, state }
  end

  and_ ~r/^I click rate button in Laptop HP-43 $/, fn state ->
    navigate_to "/ratings/givingRate/1"
    # assert visible_in_page? ~r/Time Remaining/
    {:ok, state}
  end
  and_ ~r/^I enter user email$/, fn state ->
    fill_field({:email, "email"}, state[:user_email])
    {:ok, state}
  end

  and_ ~r/^I enter score$/, fn state ->
    fill_field({:score, "score"}, state[:Score])
    {:ok, state}
  end

  when_ ~r/^I click save$/, fn state ->
    click({:id, "Submit"})
    {:ok, state}
  end

  then_ ~r/^I go to the main page$/, fn state ->
    navigate_to "/"
    {:ok, state}
  end




end
