defmodule Ratingsystem.Repo.Migrations.CreateEmail do
  use Ecto.Migration

  def change do
    create table(:email) do
      add :score, :string

      timestamps()
    end

  end
end
