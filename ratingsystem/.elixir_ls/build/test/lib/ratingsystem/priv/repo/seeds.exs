# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Ratingsystem.Repo.insert!(%Ratingsystem.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
Rating.Ratings

alias Takso.{Rating.Ratings}

[%{name: "Laptop HP-43", quantity: 25, rate: 10, votes: 4.3},
%{name: "Amazing Pants", quantity: 10, rate: 5, votes: 3.5},
%{name: "Good Pants", quantity: 25, rate: 10, votes: 4.3},
%{name: "HeadPhone X25", quantity: 10, rate: 0, votes: 0}]
|> Enum.map(fn user_data -> User.changeset(%User{}, user_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)
