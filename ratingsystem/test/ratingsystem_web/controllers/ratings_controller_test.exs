defmodule RatingsystemWeb.RatingsControllerTest do
  use RatingsystemWeb.ConnCase

  alias Ratingsystem.Rating

  @create_attrs %{product: "some product", quantity: "some quantity", rate: "some rate", votes: "some votes"}
  @update_attrs %{product: "some updated product", quantity: "some updated quantity", rate: "some updated rate", votes: "some updated votes"}
  @invalid_attrs %{product: nil, quantity: nil, rate: nil, votes: nil}

  def fixture(:ratings) do
    {:ok, ratings} = Rating.create_ratings(@create_attrs)
    ratings
  end

  describe "index" do
    test "lists all ratings", %{conn: conn} do
      conn = get conn, ratings_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Ratings"
    end
  end

  describe "new ratings" do
    test "renders form", %{conn: conn} do
      conn = get conn, ratings_path(conn, :new)
      assert html_response(conn, 200) =~ "New Ratings"
    end
  end

  describe "create ratings" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, ratings_path(conn, :create), ratings: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == ratings_path(conn, :show, id)

      conn = get conn, ratings_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Ratings"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, ratings_path(conn, :create), ratings: @invalid_attrs
      assert html_response(conn, 200) =~ "New Ratings"
    end
  end

  describe "edit ratings" do
    setup [:create_ratings]

    test "renders form for editing chosen ratings", %{conn: conn, ratings: ratings} do
      conn = get conn, ratings_path(conn, :edit, ratings)
      assert html_response(conn, 200) =~ "Edit Ratings"
    end
  end

  describe "update ratings" do
    setup [:create_ratings]

    test "redirects when data is valid", %{conn: conn, ratings: ratings} do
      conn = put conn, ratings_path(conn, :update, ratings), ratings: @update_attrs
      assert redirected_to(conn) == ratings_path(conn, :show, ratings)

      conn = get conn, ratings_path(conn, :show, ratings)
      assert html_response(conn, 200) =~ "some updated product"
    end

    test "renders errors when data is invalid", %{conn: conn, ratings: ratings} do
      conn = put conn, ratings_path(conn, :update, ratings), ratings: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Ratings"
    end
  end

  describe "delete ratings" do
    setup [:create_ratings]

    test "deletes chosen ratings", %{conn: conn, ratings: ratings} do
      conn = delete conn, ratings_path(conn, :delete, ratings)
      assert redirected_to(conn) == ratings_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, ratings_path(conn, :show, ratings)
      end
    end
  end

  defp create_ratings(_) do
    ratings = fixture(:ratings)
    {:ok, ratings: ratings}
  end
end
