defmodule RatingsystemWeb.ScoresControllerTest do
  use RatingsystemWeb.ConnCase

  alias Ratingsystem.Score

  @create_attrs %{Score: "some Score", email: "some email"}
  @update_attrs %{Score: "some updated Score", email: "some updated email"}
  @invalid_attrs %{Score: nil, email: nil}

  def fixture(:scores) do
    {:ok, scores} = Score.create_scores(@create_attrs)
    scores
  end

  describe "index" do
    test "lists all scores", %{conn: conn} do
      conn = get conn, scores_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Scores"
    end
  end

  describe "new scores" do
    test "renders form", %{conn: conn} do
      conn = get conn, scores_path(conn, :new)
      assert html_response(conn, 200) =~ "New Scores"
    end
  end

  describe "create scores" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, scores_path(conn, :create), scores: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == scores_path(conn, :show, id)

      conn = get conn, scores_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Scores"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, scores_path(conn, :create), scores: @invalid_attrs
      assert html_response(conn, 200) =~ "New Scores"
    end
  end

  describe "edit scores" do
    setup [:create_scores]

    test "renders form for editing chosen scores", %{conn: conn, scores: scores} do
      conn = get conn, scores_path(conn, :edit, scores)
      assert html_response(conn, 200) =~ "Edit Scores"
    end
  end

  describe "update scores" do
    setup [:create_scores]

    test "redirects when data is valid", %{conn: conn, scores: scores} do
      conn = put conn, scores_path(conn, :update, scores), scores: @update_attrs
      assert redirected_to(conn) == scores_path(conn, :show, scores)

      conn = get conn, scores_path(conn, :show, scores)
      assert html_response(conn, 200) =~ "some updated Score"
    end

    test "renders errors when data is invalid", %{conn: conn, scores: scores} do
      conn = put conn, scores_path(conn, :update, scores), scores: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Scores"
    end
  end

  describe "delete scores" do
    setup [:create_scores]

    test "deletes chosen scores", %{conn: conn, scores: scores} do
      conn = delete conn, scores_path(conn, :delete, scores)
      assert redirected_to(conn) == scores_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, scores_path(conn, :show, scores)
      end
    end
  end

  defp create_scores(_) do
    scores = fixture(:scores)
    {:ok, scores: scores}
  end
end
