defmodule Ratingsystem.RatingTest do
  use Ratingsystem.DataCase

  alias Ratingsystem.Rating

  describe "ratings" do
    alias Ratingsystem.Rating.Ratings

    @valid_attrs %{id: "1", email: octantyas@gmail.com , score: 5}
    @update_attrs %{product: "some updated product", quantity: "some updated quantity", rate: "some updated rate", votes: "some updated votes"}
    @invalid_attrs %{product: nil, quantity: nil, rate: nil, votes: nil}

    def ratings_fixture(attrs \\ %{}) do
      {:ok, ratings} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Rating.create_ratings()

      ratings
    end

    test "list_ratings/0 returns all ratings" do
      ratings = ratings_fixture()
      assert Rating.list_ratings() == [ratings]
    end

    test "get_ratings!/1 returns the ratings with given id" do
      ratings = ratings_fixture()
      assert Rating.get_ratings!(ratings.id) == ratings
    end

    test "create_ratings/1 with valid data creates a ratings" do
      assert {:ok, %Ratings{} = ratings} = Rating.create_ratings(@valid_attrs)
      assert ratings.product == "some product"
      assert ratings.quantity == "some quantity"
      assert ratings.rate == "some rate"
      assert ratings.votes == "some votes"
    end

    test "create_ratings/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Rating.create_ratings(@invalid_attrs)
    end

    test "update_ratings/2 with valid data updates the ratings" do
      ratings = ratings_fixture()
      assert {:ok, ratings} = Rating.update_ratings(ratings, @update_attrs)
      assert %Ratings{} = ratings
      assert ratings.product == "some updated product"
      assert ratings.quantity == "some updated quantity"
      assert ratings.rate == "some updated rate"
      assert ratings.votes == "some updated votes"
    end

    test "update_ratings/2 with invalid data returns error changeset" do
      ratings = ratings_fixture()
      assert {:error, %Ecto.Changeset{}} = Rating.update_ratings(ratings, @invalid_attrs)
      assert ratings == Rating.get_ratings!(ratings.id)
    end

    test "delete_ratings/1 deletes the ratings" do
      ratings = ratings_fixture()
      assert {:ok, %Ratings{}} = Rating.delete_ratings(ratings)
      assert_raise Ecto.NoResultsError, fn -> Rating.get_ratings!(ratings.id) end
    end

    test "change_ratings/1 returns a ratings changeset" do
      ratings = ratings_fixture()
      assert %Ecto.Changeset{} = Rating.change_ratings(ratings)
    end

    test "minute to notif for driver" do
      intended_leaving_hour = ~T[22:55:00.000000]
      minuteNotification = ParkingController.notifDriver(intended_leaving_hour)
      assert minuteNotification == false
    end



    test "get Average Rate for Product Laptop HP-43" do
      rateProduct = RatingsController.getAverageRate(id)
      assert rateProduct == 4.3
    end

    test "get Calculate Average rate for Product Laptop HP-43" do
       ratings = Rating.get_ratings!(1)
       calculateAverage == calculateAverageRate(ratings.rate, 4, ratings.id)
       assert calculateAverage == 4.27
    end

    test "get Calculate Average rate for Product with votes 0" do
      ratings = Rating.get_ratings!(4)
      calculateAverage == calculateAverageRate(ratings.rate, 0, ratings.id)
      assert calculateAverage == 0
   end


    test "update rate for Product Laptop HP-43 " do
      ratings = Rating.get_ratings!(1)
      assert {:ok, parking} = Score.create_score(@valid_attrs)
      assert %Score{} = score
      assert score.email == octantyas@gmail.com
      assert score.score == 5
    end

end
