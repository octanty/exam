defmodule Ratingsystem.ScoreTest do
  use Ratingsystem.DataCase

  alias Ratingsystem.Score

  describe "scores" do
    alias Ratingsystem.Score.Scores

    @valid_attrs %{Score: "some Score", email: "some email"}
    @update_attrs %{Score: "some updated Score", email: "some updated email"}
    @invalid_attrs %{Score: nil, email: nil}

    def scores_fixture(attrs \\ %{}) do
      {:ok, scores} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Score.create_scores()

      scores
    end

    test "list_scores/0 returns all scores" do
      scores = scores_fixture()
      assert Score.list_scores() == [scores]
    end

    test "get_scores!/1 returns the scores with given id" do
      scores = scores_fixture()
      assert Score.get_scores!(scores.id) == scores
    end

    test "create_scores/1 with valid data creates a scores" do
      assert {:ok, %Scores{} = scores} = Score.create_scores(@valid_attrs)
      assert scores.Score == "some Score"
      assert scores.email == "some email"
    end

    test "create_scores/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Score.create_scores(@invalid_attrs)
    end

    test "update_scores/2 with valid data updates the scores" do
      scores = scores_fixture()
      assert {:ok, scores} = Score.update_scores(scores, @update_attrs)
      assert %Scores{} = scores
      assert scores.Score == "some updated Score"
      assert scores.email == "some updated email"
    end

    test "update_scores/2 with invalid data returns error changeset" do
      scores = scores_fixture()
      assert {:error, %Ecto.Changeset{}} = Score.update_scores(scores, @invalid_attrs)
      assert scores == Score.get_scores!(scores.id)
    end

    test "delete_scores/1 deletes the scores" do
      scores = scores_fixture()
      assert {:ok, %Scores{}} = Score.delete_scores(scores)
      assert_raise Ecto.NoResultsError, fn -> Score.get_scores!(scores.id) end
    end

    test "change_scores/1 returns a scores changeset" do
      scores = scores_fixture()
      assert %Ecto.Changeset{} = Score.change_scores(scores)
    end
  end
end
