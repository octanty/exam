defmodule RatingsystemWeb.PageController do
  use RatingsystemWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
