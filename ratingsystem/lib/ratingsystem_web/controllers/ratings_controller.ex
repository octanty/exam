defmodule RatingsystemWeb.RatingsController do
  use RatingsystemWeb, :controller

  alias Ratingsystem.Rating
  alias Ratingsystem.Rating.Ratings

  def index(conn, _params) do
    ratings = Rating.list_ratings()
    render(conn, "index.html", ratings: ratings)
  end

  def new(conn, _params) do
    changeset = Rating.change_ratings(%Ratings{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"ratings" => ratings_params}) do
    case Rating.create_ratings(ratings_params) do
      {:ok, ratings} ->
        conn
        |> put_flash(:info, "Ratings created successfully.")
        |> redirect(to: ratings_path(conn, :show, ratings))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    ratings = Rating.get_ratings!(id)
    render(conn, "show.html", ratings: ratings)
  end

  def givingRate(conn, %{"id" => id}) do
    ratings = Rating.get_ratings!(id)
    render(conn, "giveRate.html", ratings: ratings)
  end

  def edit(conn, %{"id" => id}) do
    ratings = Rating.get_ratings!(id)
    changeset = Rating.change_ratings(ratings)
    render(conn, "edit.html", ratings: ratings, changeset: changeset)
  end

  def update(conn, %{"id" => id, "ratings" => ratings_params}) do
    ratings = Rating.get_ratings!(id)

    case Rating.update_ratings(ratings, ratings_params) do
      {:ok, ratings} ->
        conn
        |> put_flash(:info, "Ratings updated successfully.")
        |> redirect(to: ratings_path(conn, :show, ratings))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", ratings: ratings, changeset: changeset)
    end
  end

  @spec getAverageRate(any) :: any
  def getAverageRate(id) do
    ratings = Rating.get_ratings!(id)
    if(ratings.votes != 0)do
      ratings.rate
    else
      0
    end
  end


  @spec getAverageRate(any) :: any
  def calculateAverageRate(oldAverage, scoreNew, id) do
    ratings = Rating.get_ratings!(id)
    votes = ratings.votes
    newVotes = votes + 1
    newAmount = oldAverage * ratings.votes
    newScore = newAmount + scoreNew
    newAverage = newScore / newVotes

    if(newVotes != 0)do
      newAverage
    else
      0
    end
  end

  def updateRateProduct(conn, newAverageRate, newVotes, id) do
    ratings = Rating.get_ratings!(id)

    ratings_params = %{}
    ratings_params = Map.put(ratings_params, "rate", newAverageRate)
    ratings_params = Map.put(ratings_params, "newVotes", newVotes)

    case Rating.update_ratings(ratings_params, ratings) do
      {:ok, ratings} ->
        conn
        |> put_flash(:info, "Ratings updated successfully.")
        |> redirect(to: ratings_path(conn, :show, ratings))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", ratings: ratings, changeset: changeset)
    end
  end

  def updateRating(conn, %{"id" => id, "ratings" => ratings_params}) do
    ratings = Rating.get_ratings!(id)
    email = Map.get(ratings_params, "email" )
    score_as_string = Map.get(ratings_params, "score")
    score = String.to_integer(score_as_string)
    oldAverage = getAverageRate(id)
    newAverageRate = calculateAverageRate(oldAverage, score, id)
    newVotes = ratings.votes + 1

    updateRateProduct(conn, newAverageRate, newVotes, ratings.id)
    ratings_params = Map.put(ratings_params, "email", email)
    ratings_params = Map.put(ratings_params, "score", score)
    ratings_params = Map.put(ratings_params, "product_id", ratings.id)
    if email != nil && score != nil && score >=0 && score <=5 do
      case Score.create_score(ratings_params) do
        {:ok, parking} ->
          conn
          |> put_flash(:info, "Score inserted successfully.")
          |> redirect(to: ratings_path(conn, :index, ratings))
          {:error, %Ecto.Changeset{} = changeset} ->
          render(conn, "index.html", changeset: changeset)
      end
    end

end



  def delete(conn, %{"id" => id}) do
    ratings = Rating.get_ratings!(id)
    {:ok, _ratings} = Rating.delete_ratings(ratings)

    conn
    |> put_flash(:info, "Ratings deleted successfully.")
    |> redirect(to: ratings_path(conn, :index))
  end
end
