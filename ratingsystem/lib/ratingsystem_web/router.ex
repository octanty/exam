defmodule RatingsystemWeb.Router do
  use RatingsystemWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", RatingsystemWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    resources "/ratings", RatingsController
    get "/ratings/givingRate/:id", RatingsController, :givingRate
    get "/ratings/updateRating/:id", RatingsController, :updateRating
  end

  # Other scopes may use custom stacks.
  # scope "/api", RatingsystemWeb do
  #   pipe_through :api
  # end
end
