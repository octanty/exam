defmodule Ratingsystem.Score do
  @moduledoc """
  The Score context.
  """

  import Ecto.Query, warn: false
  alias Ratingsystem.Repo

  alias Ratingsystem.Score.Scores

  @doc """
  Returns the list of scores.

  ## Examples

      iex> list_scores()
      [%Scores{}, ...]

  """
  def list_scores do
    Repo.all(Scores)
  end

  @doc """
  Gets a single scores.

  Raises `Ecto.NoResultsError` if the Scores does not exist.

  ## Examples

      iex> get_scores!(123)
      %Scores{}

      iex> get_scores!(456)
      ** (Ecto.NoResultsError)

  """
  def get_scores!(id), do: Repo.get!(Scores, id)

  @doc """
  Creates a scores.

  ## Examples

      iex> create_scores(%{field: value})
      {:ok, %Scores{}}

      iex> create_scores(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_scores(attrs \\ %{}) do
    %Scores{}
    |> Scores.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a scores.

  ## Examples

      iex> update_scores(scores, %{field: new_value})
      {:ok, %Scores{}}

      iex> update_scores(scores, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_scores(%Scores{} = scores, attrs) do
    scores
    |> Scores.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Scores.

  ## Examples

      iex> delete_scores(scores)
      {:ok, %Scores{}}

      iex> delete_scores(scores)
      {:error, %Ecto.Changeset{}}

  """
  def delete_scores(%Scores{} = scores) do
    Repo.delete(scores)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking scores changes.

  ## Examples

      iex> change_scores(scores)
      %Ecto.Changeset{source: %Scores{}}

  """
  def change_scores(%Scores{} = scores) do
    Scores.changeset(scores, %{})
  end
end
