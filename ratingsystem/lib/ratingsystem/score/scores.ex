defmodule Ratingsystem.Score.Scores do
  use Ecto.Schema
  import Ecto.Changeset


  schema "scores" do
    field :Score, :string
    field :email, :string

    timestamps()
  end

  @doc false
  def changeset(scores, attrs) do
    scores
    |> cast(attrs, [:email, :Score])
    |> validate_required([:email, :Score])
  end
end
