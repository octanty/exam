defmodule Ratingsystem.Rating.Ratings do
  use Ecto.Schema
  import Ecto.Changeset


  schema "ratings" do
    field :product, :string
    field :quantity, :integer

    timestamps()
  end

  @doc false
  def changeset(ratings, attrs) do
    ratings
    |> cast(attrs, [:product, :quantity, :votes, :rate])
    |> validate_required([:product, :quantity, :votes, :rate])
  end
end
