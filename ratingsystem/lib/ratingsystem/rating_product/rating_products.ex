defmodule Ratingsystem.RatingProduct.RatingProducts do
  use Ecto.Schema
  import Ecto.Changeset


  schema "rate_product" do
    field :email, :string, primary_key: true
    field :score, :string

    timestamps()
  end

  @doc false
  def changeset(rating_products, attrs) do
    rating_products
    |> cast(attrs, [:email, :score])
    |> validate_required([:email, :score])
  end
end
