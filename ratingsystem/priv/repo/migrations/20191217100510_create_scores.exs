defmodule Ratingsystem.Repo.Migrations.CreateScores do
  use Ecto.Migration

  def change do
    create table(:scores) do
      add :email, :string, primary_key: true
      add :Score, :integer
      add :productID, :string

      timestamps()
    end

  end
end
