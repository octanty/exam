defmodule Ratingsystem.Repo.Migrations.CreateRateProduct do
  use Ecto.Migration

  def change do
    create table(:rate_product) do
      add :email, :string, primary_key: true
      add :score, :string

      timestamps()
    end

  end
end


