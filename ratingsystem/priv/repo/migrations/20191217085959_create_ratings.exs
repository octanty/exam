defmodule Ratingsystem.Repo.Migrations.CreateRatings do
  use Ecto.Migration

  def change do
    create table(:ratings) do
      add :product, :string
      add :quantity, :integer
      add :votes, :integer
      add :rate, :float

      timestamps()
    end

  end
end
